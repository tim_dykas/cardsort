# Card Sort

This is a based on an activity from the **Computer Science Discoveries** 2019-20 curriculum from [Code.org](code.org) (it has since been replaced) where students were given several cards from a standard deck of playing cards. One student would act as the _sorter_ and the other as the _pointer_. The cards would be face down and the _sorter_ wanted to get them into increasing order. The _sorter_ could pick up, put, down, and swap and cards she wanted. She could also show two cards to the _pointer_, who would point at the larger value.

This program makes this activity one-player. The user acts as the sorter and program acts as the pointer (as well as the cards themselves).

The user can choose how many cards there will be to sort.

![Start Screen](https://drive.google.com/uc?export=download&id=11gC4J0NNEFsHbUknOgt24fgw14EcqtyT)

Each card is generated as a button.

![Play Screen](https://drive.google.com/uc?export=download&id=11grsROz2WWPFsqd2hzICjwXRlNAJST86)

When two cards are selected, the user is show which card has a greater value and has the option to swap the cards or to put them down. The user can also choose to show the values of the cards to test their process or to check that the cards are sorted at the end.

![Cards Selected](https://drive.google.com/uc?export=download&id=11lEwDGQlomU03_YFZWU-ce4VvL7lkEnQ)

When the cards are swapped (or put down) the user can now select two more cards.

![Cards Swapped](https://drive.google.com/uc?export=download&id=11lbX-Ern2NXTvUnqypSwUFXiqP6NHTZG)

The user can start a new game at anytime by clicking the _New Game_ button. The layout adjusts even for many cards.

![Many Cards](https://drive.google.com/uc?export=download&id=11pJYVGEUjV28ZvtxfTvYNzZV_J91hH5U)