import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class Main {

	static int numCards, numSelected, firstSelected;
	static Color selectedColor = new Color(255, 0, 0);
	static Color defaultColor = null;
	static int[] values;
	static JPanel gamePanel, optionsPanel, cardPanel;
	static JButton[] buttons;
	static boolean showValues = false;

	static void redraw(Container frame) {
		setSize();
		frame.revalidate();
		frame.repaint();
	}

	static void setSize() {
		optionsPanel.setMaximumSize(optionsPanel.getPreferredSize());
	}
	
	static String getButtonText(int index) {
		return "<html><center>" + (index + 1) + (showValues ? "<br>" + values[index] : "") + "</center></html>";
	}

	static void initPanel() {
		gamePanel = new JPanel();
		gamePanel.setLayout(new BoxLayout(gamePanel, BoxLayout.Y_AXIS));
		optionsPanel = new JPanel();
		cardPanel = new JPanel();
		gamePanel.add(optionsPanel);
		gamePanel.add(cardPanel);
	}
	
	static void onOptionButtonClick(JComponent[] compList, int index) {
		buttons[firstSelected].setBackground(defaultColor);
		buttons[index].setBackground(defaultColor);
		
		for(JComponent comp : compList) {
			optionsPanel.remove(comp);
		}
		redraw(gamePanel);
		numSelected = 0;
	}
	
	static void startGame() {
		numSelected = 0;

		JLabel numCardsFieldLabel = new JLabel("<html>Input the number of cards and press <i>Enter</i>");
		optionsPanel.add(numCardsFieldLabel);
		JTextField numCardsField = new JTextField(10);
		// numCardsField.setSize(100, 40);
		numCardsField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				numCardsField.selectAll();
				try {
					numCards = Integer.parseInt(numCardsField.getText());
				} catch (NumberFormatException err) {
					return;
				}
				values = new int[numCards];
				buttons = new JButton[numCards];
				Random r = new Random();

				JCheckBox showValuesBox = new JCheckBox("Show Values");
				showValuesBox.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						showValues = showValuesBox.isSelected();

						for (int i = 0; i < buttons.length; i++) {
							buttons[i].setText(getButtonText(i));
						}
					}
				});
				optionsPanel.add(showValuesBox);

				for (int i = 0; i < numCards; i++) {
					final int index = i;
					values[i] = r.nextInt(13) + 1;
					JButton b = new JButton(getButtonText(i));
					buttons[i] = b;
					b.setHorizontalAlignment(SwingConstants.CENTER);
					b.setHorizontalTextPosition(SwingConstants.CENTER);
					b.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							System.out.println("Action");
							numSelected++;
							if (numSelected == 1) {

								b.setBackground(selectedColor);
								firstSelected = index;
							} else if (numSelected == 2) {

								b.setBackground(selectedColor);

								JLabel higherLabel = new JLabel();
								if (values[firstSelected] > values[index]) {
									System.out.println("First greater");
									higherLabel.setText((firstSelected + 1) + " is greater.");
								} else if (values[firstSelected] <= values[index]) {
									System.out.println("Second greater");
									higherLabel.setText((index + 1) + " is greater.");
								}
								optionsPanel.add(higherLabel);

								JButton swapButton = new JButton("Swap");
								JButton putDownButton = new JButton("Put Down");

								swapButton.addActionListener(new ActionListener() {

									@Override
									public void actionPerformed(ActionEvent e) {
										int holder = values[firstSelected];
										values[firstSelected] = values[index];
										values[index] = holder;
										
										buttons[firstSelected].setText(getButtonText(firstSelected));
										buttons[index].setText(getButtonText(index));
										
										onOptionButtonClick(new JComponent[]{higherLabel,swapButton,putDownButton}, index);
									}
								});
								optionsPanel.add(swapButton);

								putDownButton.addActionListener(new ActionListener() {

									@Override
									public void actionPerformed(ActionEvent e) {
										onOptionButtonClick(new JComponent[]{higherLabel,swapButton,putDownButton}, index);
									}
								});
								optionsPanel.add(putDownButton);

								redraw(gamePanel);
							}

						}
					});

					cardPanel.add(b);
				}
				cardPanel.remove(numCardsField);
				optionsPanel.remove(numCardsFieldLabel);
				redraw(gamePanel);

			}
		});
		cardPanel.add(numCardsField);
		setSize();

	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new JFrame("Card Sort");
				frame.setLayout(new BorderLayout());
				frame.setSize(500, 200);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);

				initPanel();

				JButton resetButton = new JButton("New Game");
				resetButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						showValues = false;
						frame.remove(gamePanel);
						initPanel();
						frame.add(gamePanel, BorderLayout.CENTER);
						startGame();
						redraw(frame.getContentPane());
					}
				});
				frame.add(resetButton, BorderLayout.PAGE_START);

				frame.add(gamePanel, BorderLayout.CENTER);
				startGame();

			}

		});

	}

}